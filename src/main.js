import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource';

import List from './components/List.vue'
import ListItemDetail from './components/ListItemDetail.vue'
import EditListItem from './components/EditListItem.vue'

import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(VueResource);
Vue.use(VueMaterial)

Vue.component('router-link', Vue.options.components.RouterLink);
Vue.component('router-view', Vue.options.components.RouterView);

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [
  { path: '/', component: List },
  { path: '/item/:id', name: 'item', component: ListItemDetail },
  { path: '/item/:id/update', name: 'updateItem', component: EditListItem }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes,
  mode: 'history'

})


new Vue({
  http: {
    root: '/root',
    headers: {}
  },	
  router,
  render: h => h(App),
}).$mount('#app')
